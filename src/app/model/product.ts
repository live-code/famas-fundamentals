export type Role = 'admin' | 'guest';

export interface Product {
  id: number | null;
  name: string;
  cost: number;
  role?: Role;
}

