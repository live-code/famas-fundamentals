import { Injectable } from '@angular/core';

type Theme = 'dark' | 'light';

@Injectable()
export class ThemeService {
  private _theme: Theme = 'light'

  constructor() {
    this._theme = localStorage.getItem('theme') as Theme
  }

  set theme(theme: Theme ) {
    this._theme = theme;
    localStorage.setItem('theme', theme)
  }

  get theme(): Theme {
    return this._theme;
  }
}

