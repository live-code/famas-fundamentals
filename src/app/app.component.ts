import { Component } from '@angular/core';
import { ThemeService } from './core/theme.service';

@Component({
  selector: 'fm-root',
  template: `
    
    <div 
      class="p-3" 
      [ngClass]="{
        'bg-dark': themeService.theme === 'dark',
        'bg-light': themeService.theme === 'light'
      }"
    >
      <button routerLink="catalog" routerLinkActive="bg-warnin">catalog</button>
      <button routerLink="home" routerLinkActive="bg-warning">home</button>
      <button routerLink="login" routerLinkActive="bg-warning">login</button>
      <button routerLink="settings" routerLinkActive="bg-warning">settings</button>
      <button routerLink="contacts" routerLinkActive="bg-warning">contacts</button>
    </div>
  
    <hr>
    
    <router-outlet></router-outlet>
  `,

})
export class AppComponent {
  constructor(public themeService: ThemeService) {
  }
}

