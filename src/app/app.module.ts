import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CatalogComponent } from './features/catalog/catalog.component';
import { HomeComponent } from './features/home/home.component';
import { LoginComponent } from './features/login/login.component';
import { RouterModule } from '@angular/router';
import { CatalogDetailsComponent } from './features/catalog-details/catalog-details.component';
import { SettingsComponent } from './features/settings/settings.component';
import { SharedModule } from './shared/shared.module';
import { ThemeService } from './core/theme.service';
import { LogService } from './core/log.service';
import { LogFakeService } from './core/log-fake.service';
import {
  AdminToolsClient,
  AnagraficaClient,
  AnalisiDatiClient,
  AssetsClient,
  AuthClient,
  AutodiagnosiClient, CampagneClient, DatiTrattaClient, DiagnosticaDashboardClient
} from './api';

@NgModule({
  declarations: [
    AppComponent, HomeComponent,
    LoginComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SharedModule.forRoot(),
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent},
      { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule)},
      { path: 'catalog/:productId', component: CatalogDetailsComponent},
      { path: 'settings', component: SettingsComponent},
      { path: 'home', component: HomeComponent},
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) },
      { path: '**', redirectTo: 'home'},
    ])
  ],
  providers: [
    // ThemeService
    LogService,

    { provide: ThemeService, useClass: ThemeService},
    // { provide: LogService, useClass: XXXService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
