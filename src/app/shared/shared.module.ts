import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './components/panel.component';
import { HelloComponent } from './components/hello.component';
import { CardComponent } from './components/card.component';
import { TabbarComponent } from './components/tabbar.component';
import { CounterService } from './service/counter.service';



@NgModule({
  declarations: [
    PanelComponent,
    HelloComponent,
    CardComponent,
    TabbarComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PanelComponent,
    HelloComponent,
    CardComponent,
    TabbarComponent
  ],
  providers: [

  ]
})
export class SharedModule {
  static forRoot() : ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        CounterService
      ]
    }
  }
}
