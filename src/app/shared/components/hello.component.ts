import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fm-hello',
  template: `
    <p>
      hello {{name}}!
    </p>
  `,
  styles: [
  ]
})
export class HelloComponent {
  @Input() name: string | number = 'Fabio'
}
