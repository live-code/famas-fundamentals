import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'fm-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item" *ngFor="let tab of data">
        <a 
          class="nav-link"
          [ngClass]="{'active': tab.id === active?.id}"
          (click)="tabClick.emit(tab)"
        >
          {{tab.label}}
        </a>
      </li>
    </ul>
  `,
  styles: [
  ]
})
export class TabbarComponent {
  @Input() data!: any[];
  @Output() tabClick = new EventEmitter<any>();
  @Input() active: any;
}
