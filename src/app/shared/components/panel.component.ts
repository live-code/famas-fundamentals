import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'fm-panel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    {{title}}: {{theme}}
  `
})
export class PanelComponent {
  @Input() title: string = 'theme';
  @Input() theme: string = 'theme';
}

