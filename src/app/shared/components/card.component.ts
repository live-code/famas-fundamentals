import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'fm-card',
  template: `
    <div class="card" [ngClass]="'mb-' + marginBottom">
      <div *ngIf="title" class="card-header" [ngClass]="getHeaderCls()"
           (click)="open = !open">
        {{title}}
        <div class="pull-right">
          <i
            (click)="iconClickHandler($event)"
            *ngIf="icon" class="fa" [ngClass]="icon"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="open">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class CardComponent  {
  @Input() title: string | null = null;
  @Input() icon: string | null = null;
  @Input() marginBottom: 0 | 1 | 2 | 3 | 4 | 5  = 0
  @Input() alertType: 'danger' | 'warning' | 'success' | 'dark' | 'light' | null = null
  @Output() iconClick = new EventEmitter()
  open = true;

  iconClickHandler(event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit()
  }

  getHeaderCls() {
    if (this.alertType) {
      return 'bg-' + this.alertType
    }
    return '';
  }
}
