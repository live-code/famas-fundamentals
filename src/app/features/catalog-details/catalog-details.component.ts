import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../../model/product';
import { switchMap } from 'rxjs/operators';
import { ProductsService } from '../catalog/services/products.service';

@Component({
  selector: 'fm-catalog-details',
  template: `
    <div *ngIf="pending">loading...</div>
    <h1>{{product?.name}}</h1>
    <h1>{{product?.cost}}</h1>
    
    <button routerLink="../">back</button>
    <button (click)="gotoNext()">Next</button>
  `,
  styles: [
  ]
})
export class CatalogDetailsComponent {
  product: Product | null = null;
  pending = true;
  // currentId!: string;
  nextIndex!: number;

  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productsService: ProductsService
  ) {
    // SNAPTHOS
    // if (this.currentId) {
    //   this.http.get<Product>(`http://localhost:3000/products/${this.currentId}`)
    //     .subscribe(res => {
    //       this.product = res;
    //     })
    // }

    // OBSERVABLE
    /*this.activatedRoute.params
      .subscribe(params => {
        this.http.get<Product>(`http://localhost:3000/products/${params.productId}`)
             .subscribe(res => {
               this.product = res;
             })
      })*/

    this.activatedRoute.params
      .pipe(
        switchMap(params =>  this.http.get<Product>(`http://localhost:3000/products/${params.productId}`))
      )
      .subscribe(res => {
          const id = res?.id
          this.product = res;
          this.pending = false;
          if (id) {
            const currentID = productsService.products.findIndex(p => p.id === id );
            if (currentID !== -1) {
              this.nextIndex = currentID + 1
              console.log(this.nextIndex)
            } else {
              this.productsService.getProducts()
                .then(products => {
                  const currentID = products.findIndex(p => p.id === id );
                  this.nextIndex = currentID + 1
                })

            }
          }
      })
  }

  gotoNext() {
    // this.currentId = (+this.currentId + 1).toString();
    // this.router.navigateByUrl('/catalog/' + this.currentId)

    this.router.navigateByUrl('/catalog/' + this.productsService.products[this.nextIndex].id)
  }
}
