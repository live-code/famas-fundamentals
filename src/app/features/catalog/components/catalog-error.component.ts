import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fm-catalog-error',
  template: `
    <div class="alert alert-danger" 
         *ngIf="error">errore</div>
  `,
  styles: [
  ]
})
export class CatalogErrorComponent implements OnInit {
  @Input() error: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
