import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../../../model/product';

@Component({
  selector: 'fm-catalog-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li
      class="list-group-item"
      [ngClass]="{'active': selected}"
      (click)="selectItem.emit()"
    >
      
      <div class="d-flex justify-content-between align-items-center">
        <div>
          <div>
            {{product.name}} - {{product.cost}} 
          </div>
         
          <div>
            bla bla
          </div>
        </div>

        <div>
          {{product.id}}
        </div>
        
        
        <div>
          <div class="pull-right">
            <i class="fa fa-link" (click)="goto.emit()"></i>
            <i class="fa" [ngClass]="{'fa-arrow-circle-up': opened, 'fa-arrow-circle-down': !opened}"
               (click)="toggle.emit(product)"></i>
            <i class="fa fa-times"
               (click)="deleteHandler($event)"></i>
          </div>
        </div>
      </div>
    </li>
  `,
  styles: [
  ]
})
export class CatalogListItemComponent {
  @Input() product!: Partial<Product>;
  @Input() selected: boolean = false;
  @Input() opened: boolean = false;
  @Output() toggle = new EventEmitter<Partial<Product>>();
  @Output() selectItem = new EventEmitter();
  @Output() deleteItem = new EventEmitter();
  @Output() goto = new EventEmitter();
  // @Output() openedChanged = new EventEmitter();

  deleteHandler(event: MouseEvent) {
    event.stopPropagation();
    this.deleteItem.emit();
  }
}
