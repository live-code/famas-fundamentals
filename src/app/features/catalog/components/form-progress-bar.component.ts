import { Component, Input } from '@angular/core';
import { NgModel } from '@angular/forms';
import { getPerc } from '../utils/form-utils';

@Component({
  selector: 'fm-form-progress-bar',
  template: `
    <div
      class="progressBar"
      [style.width.%]="getPerc(inputRef)"
      [style.background]="getBg(inputRef)"
    ></div>
  `,
  styles: [`
    .progressBar {
      background-color: red;
      height: 5px;
      transition: 0.5s width ease-in-out ;
    }
  `]
})
export class FormProgressBarComponent {
  @Input() inputRef!: NgModel;

  getPerc(input: NgModel) {
    return getPerc(input.errors?.minlength?.actualLength, input.errors?.minlength?.requiredLength);
  }

  getBg(input: NgModel) {
    const perc = getPerc(input.errors?.minlength?.actualLength, input.errors?.minlength?.requiredLength)
    return perc < 50 ? 'red' : 'orange';
  }
}
