import { EventEmitter, Component, Input, OnInit, Output, ChangeDetectionStrategy } from '@angular/core';
import { Product } from '../../../model/product';

@Component({
  selector: 'fm-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    
    <fm-catalog-list-item
      *ngFor="let p of products; let i = index"
      [product]="p"
      [selected]="active?.id === p.id"
      [opened]="openedProduct?.id === p.id"
      (selectItem)="selectItem.emit(p)"
      (deleteItem)="deleteItem.emit(p)"
      (goto)="goto.emit(p)"
      (toggle)="setOpenToggle(p)"
    ></fm-catalog-list-item>
    
  `,
})
export class CatalogListComponent {
  @Input() products: Partial<Product>[] = [];
  @Input() active: Partial<Product> | null = null;
  @Output() selectItem = new EventEmitter<Partial<Product>>();
  @Output() deleteItem = new EventEmitter<Partial<Product>>();
  @Output() goto = new EventEmitter<Partial<Product>>();

  openedProduct: Partial<Product> | null = null;

  setOpenToggle(p: Partial<Product>) {
    this.openedProduct = (p.id === this.openedProduct?.id) ? null : p
  }
}
