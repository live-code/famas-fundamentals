import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { ProductsService } from '../services/products.service';
import { NgForm } from '@angular/forms';
import { Product } from '../../../model/product';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'fm-catalog-form',
  template: `
    <form
      #f="ngForm" (submit)="save.emit(f.value)"
    >
      
      <input
        type="text"
        class="form-control"
        [ngClass]="{'is-invalid': inputName.invalid && f.dirty}"
        name="name"
        [ngModel]="activeProduct?.name"
        #inputRef
        #inputName="ngModel"
        required minlength="3"
      >

      <fm-form-progress-bar [inputRef]="inputName"></fm-form-progress-bar>

      <div *ngIf="inputName.errors?.required">il campo è obbligatorio</div>
      <div *ngIf="inputName.errors?.minlength">
        Mancano {{inputName.errors?.minlength.requiredLength - inputName.errors?.minlength.actualLength}}  caratteri
      </div>

      <input
        type="email"
        class="form-control"
        [ngClass]="{'is-invalid': inputCost.invalid && f.dirty}"
        name="cost" [ngModel]="activeProduct?.cost" required
        [pattern]="regEx"
        #inputCost="ngModel"
      >

      <div class="btn-group">
        <button class="btn btn-outline-primary" type="submit" [disabled]="f.invalid">
          {{activeProduct?.id ? 'EDIT' : 'ADD'}}
        </button>
        <button
          *ngIf="activeProduct"
          class="btn btn-secondary" type="button" 
          (click)="clear.emit()">CLEAR
        </button>
      </div>
    </form>
  `,
  styles: [
  ]
})
export class CatalogFormComponent implements OnChanges, AfterViewInit {
  @ViewChild('f', { static: true }) form!: NgForm;
  @ViewChild('inputRef', { static: true }) inputRef!: ElementRef<HTMLInputElement>;

  @Input() activeProduct: Partial<Product> | null = null;
  @Output() clear = new EventEmitter();
  @Output() save = new EventEmitter<Partial<Product>>();
  regEx = /^[0-9]*$/;

  ngOnChanges() {
    if (!this.activeProduct?.id) {
       this.form.reset()
    }

  }

  ngAfterViewInit() {
    this.inputRef.nativeElement.focus()
  }
}
