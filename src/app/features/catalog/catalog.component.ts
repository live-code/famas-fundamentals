import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProductsService } from './services/products.service';
import { Product } from '../../model/product';
import { CatalogFormComponent } from './components/catalog-form.component';
import { Router } from '@angular/router';
import { LogService } from '../../core/log.service';
import { CounterService } from '../../shared/service/counter.service';

@Component({
  selector: 'fm-catalog',

  template: `
    <div>
      <fm-catalog-error [error]="productsService.error"></fm-catalog-error>
      
      <fm-card [title]="productsService.activeProduct.id ? 'EDIT ' + productsService.activeProduct.name : 'ADD PRODUCT'" [marginBottom]="4">
        <fm-catalog-form
          [activeProduct]="productsService.activeProduct"
          (clear)="productsService.clear()"
          (save)="save($event)"
        ></fm-catalog-form>
      </fm-card>

     <fm-card [title]="productsService.products.length + ' products'">
        <fm-catalog-list
          [products]="productsService.products"
          [active]="productsService.activeProduct"
          (goto)="changePage($event)"
          (selectItem)="productsService.selectItem($event)"
          (deleteItem)="productsService.deleteHandler($event)"
        ></fm-catalog-list>
     </fm-card>
    </div>
    <button (click)="counterService.inc()">{{counterService.value}}</button>

  `,
  providers: [ProductsService]

})
export class CatalogComponent implements OnInit {
  constructor(
    public productsService: ProductsService,
    private router: Router,
    private log: LogService,
    public counterService: CounterService
  ) {}

  ngOnInit(): void {
    this.productsService.getProducts()
      .then(res => {
        console.log(res)
      })
  }

  save(formData: Partial<Product>) {
    this.productsService.save(formData)
      .then((res) => {
        if (res.action === 'add') {
          // f.reset()
          // this.form.resetHandler()
        }
      })
  }


  changePage(product: Partial<Product>) {
    //... save anything in service
    this.router.navigateByUrl('/catalog/' + product.id)
  }

/*
  ngOnInit2(): void {
    forkJoin({
      products: this.http.get<Product[]>('http://localhost:3000/products'),
      cats: this.http.get<any[]>('http://localhost:3000/cats')
    })
      .subscribe(console.log)

    this.http.get<Product[]>('http://localhost:3000/products')
      /!*.pipe(
        switchMap(products => products),
        filter(product => {
          return product.cost > 10
        }),
        toArray()
      )*!/
      .pipe(
        switchMap(products => {

          return this.http.get<any[]>('http://localhost:3000/cats')
            .pipe(
              map(cats => {
                return {
                  products: products,
                  cats: cats
                }
              })
            )
        })
      )
      .subscribe(res => {
        console.log(res)
        this.products = res.products
      })
  }
*/

/*
  deleteHandler(p: Partial<Product>) {
    if (p.id) {
      this.productsService.deleteHandler(p.id)
        .subscribe(res => {
          alert('qui!')
        })
    }
  }*/

}

