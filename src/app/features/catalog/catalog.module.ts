import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogListComponent } from './components/catalog-list.component';
import { FormProgressBarComponent } from './components/form-progress-bar.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogErrorComponent } from './components/catalog-error.component';
import { CatalogListItemComponent } from './components/catalog-list-item.component';
import { CatalogDetailsComponent } from '../catalog-details/catalog-details.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CatalogComponent } from './catalog.component';
import { SharedModule } from '../../shared/shared.module';
import { LogService } from '../../core/log.service';
import { LogFakeService } from '../../core/log-fake.service';
import { environment } from '../../../environments/environment';
import { ProductsService } from './services/products.service';
import {
  AdminToolsClient,
  AnagraficaClient,
  AnalisiDatiClient,
  AssetsClient,
  AuthClient,
  AutodiagnosiClient, CampagneClient, DatiTrattaClient, DiagnosticaDashboardClient
} from '../../api';



@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent, FormProgressBarComponent, CatalogFormComponent,
    CatalogErrorComponent, CatalogListItemComponent, CatalogDetailsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: CatalogComponent }
    ])
  ],
  providers: [
    AdminToolsClient,
    AnagraficaClient,
    AnalisiDatiClient,
    AssetsClient,
    AuthClient,
    AutodiagnosiClient,
    CampagneClient,
    DatiTrattaClient,
    DiagnosticaDashboardClient,
  ]
})
export class CatalogModule { }
