
export function getPerc(current: number, max: number) {
  if (!current || !max) {
    return 0
  }
  return (current / max) * 100;
}

