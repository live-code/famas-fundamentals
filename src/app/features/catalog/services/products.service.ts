import { Injectable } from '@angular/core';
import { Product } from '../../../model/product';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {  finalize, shareReplay } from 'rxjs/operators';

type UpsertPromise = Promise<{ data: Product, action: 'edit' | 'add' }>

@Injectable()
export class ProductsService {
  products: Partial<Product>[] = [];
  activeProduct: Partial<Product> = {};
  error: boolean = false;

  constructor(private http: HttpClient) {}

  getProducts(): Promise<Product[]> {
    return this.http.get<Product[]>('http://localhost:3000/products')
      .toPromise()
      .then(
        res => {
          this.products = res;
          return res;
        },
        err => {
          console.log('error')
          this.error = true;
          return err;
        }
      )
  }

  deleteHandler(product: Partial<Product>): Observable<Object> {
   const req = this.http.delete(`http://localhost:3000/products/${product.id}`)
      .pipe(
        shareReplay(1),
        finalize(() => {
          // console.log('completed or error')
        })
      )

    req.subscribe(
        () => {
          this.products = this.products.filter(p => p.id !== product.id);
          if (product.id === this.activeProduct?.id) {
            this.activeProduct = {};
          }
        },
        err => {
          console.log(err)
        },
      )

    return req;
  }


  save(product: Partial<Product>): UpsertPromise {
    if (this.activeProduct?.id) {
      return this.editProduct(product)
    } else {
      return this.addProduct(product)
    }
  }


  addProduct(product: Partial<Product>): UpsertPromise {
    return new Promise((resolve, reject) => {
      this.http.post<Product>(`http://localhost:3000/products/`, product)
        .subscribe(product => {
          // const product: Product = { ...form.value, id: Date.now()}
          this.products = [...this.products, product];
          this.activeProduct = {}
          resolve({
            data: product,
            action: 'add'
          });
          // form.reset(); // TO FIX
        })
    })

  }

  editProduct(product: Partial<Product>): UpsertPromise {
    return new Promise((resolve, reject) => {
      this.http.patch<Product>(`http://localhost:3000/products/${this.activeProduct?.id}`, product)
        .subscribe(
          product => {
            this.products = this.products.map(p => {
              return p.id === this.activeProduct?.id ? product : p;
            })
            this.activeProduct = {};
            resolve({
              data: product,
              action: 'edit'
            });
          },
          (err) => {
            reject(err)
          })
    })

  }

  clear() {
    this.activeProduct = { };
  }

  selectItem(p: Partial<Product>) {
    this.activeProduct = p;
  }

}
