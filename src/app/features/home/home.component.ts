import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/theme.service';
import { CounterService } from '../../shared/service/counter.service';

@Component({
  selector: 'fm-home',
  template: `
    
    <h1>{{yourname}}</h1>
    
    <button (click)="selectedTab = countries[2]">go to germany</button>
    
    <fm-tabbar
      [data]="countries"
      [active]="selectedTab"
      (tabClick)="doSomething($event)"
    ></fm-tabbar>
    
    <hr>
    <fm-card 
      [title]="selectedTab?.label || ''"
      [marginBottom]="2"
      [alertType]="selectedTab?.id === 1 ? 'danger' : null"
      icon="fa-link" 
      (iconClick)="openUrl('https://en.wikipedia.org/wiki/' + selectedTab?.label)"
    >
      
      {{selectedTab?.desc}}
    </fm-card>
    
    <fm-card 
      title="uno" 
      icon="fa-facebook" (iconClick)="openUrl('http://www.google.com')"
      [alertType]="themeService.theme"
    >
      <button>ciao</button>
    </fm-card>

    <fm-card title="due">
      <div class="row">
        <div class="col">
          <fm-card title="due">
            <button>1</button>
          </fm-card>
        </div>
        <div class="col">
          <fm-card title="due">
            <button>1</button>
          </fm-card>
        </div>
      </div>
    </fm-card>
    <button (click)="counterService.inc()">{{counterService.value}}</button>

  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  yourname = 'Pippo';
  selectedTab: Country | null = null;
  countries: Country[] = []

  constructor(public themeService: ThemeService, public counterService: CounterService) {}

    ngOnInit(): void {

    setTimeout(() => {
      this.countries = [
        { id: 1, label: 'Italy', desc: 'bla bla 1'},
        { id: 2, label: 'Spain', desc: 'bla bla 2'},
        { id: 3, label: 'Germany', desc: 'bla bla 3'},
      ]

      this.selectedTab = this.countries[2]
    }, 3000)
  }

  openUrl(url: string) {
    window.open(url)
  }

  doSomething(tab: Country) {
    console.log('do something', tab)
    this.selectedTab = tab;
  }
}


interface Country {
  id: number;
  label: string;
  desc: string;
}
