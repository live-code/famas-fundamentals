import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../catalog/services/products.service';
import { CounterService } from '../../shared/service/counter.service';

@Component({
  selector: 'fm-login',
  template: `
    <p>
      login works!
    </p>
    
    <button (click)="counterService.inc()">{{counterService.value}}</button>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor(public counterService: CounterService) { }

  ngOnInit(): void {
  }

}
