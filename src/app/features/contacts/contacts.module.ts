import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ContactsComponent } from './contacts.component';
import {
  AdminToolsClient,
  AnagraficaClient,
  AnalisiDatiClient,
  AssetsClient,
  AuthClient,
  AutodiagnosiClient, CampagneClient, DatiTrattaClient, DiagnosticaDashboardClient
} from '../../api';


const routes: Routes = [
  { path: '', component: ContactsComponent }
];

@NgModule({
  declarations: [
    ContactsComponent
  ],
  providers: [
    AdminToolsClient,
    AnagraficaClient,
    AnalisiDatiClient,
    AssetsClient,
    AuthClient,
    AutodiagnosiClient,
    CampagneClient,
    DatiTrattaClient,
    DiagnosticaDashboardClient,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ContactsModule { }
