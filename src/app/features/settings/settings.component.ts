import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/theme.service';
import { LogService } from '../../core/log.service';
import { environment } from '../../../environments/environment';
import { CounterService } from '../../shared/service/counter.service';

@Component({
  selector: 'fm-settings',
  template: `
    <button (click)="themeService.theme = 'dark'">set dark</button>
    <button (click)="themeService.theme = 'light'">set light</button>
  `,
  styles: [
  ]
})
export class SettingsComponent {


  constructor(public themeService: ThemeService, private log: LogService) {
    console.log(themeService.theme, environment.baseAPI)
  }
}
